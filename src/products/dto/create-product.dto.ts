import { IsInt, Min, IsNotEmpty, MinLength } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  price: number;
}
